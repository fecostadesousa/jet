# Marketing Digital - SEO Friendly - JET / NEO
![alt text](https://felipercsilva.net.br/wp-content/uploads/2013/02/prouncia-seo.jpg)

SEO - Search Engine Optimization
    
  - 78,5% das empresas concentram a função de elaborar e testar uma estratégia de SEO apenas entre a sua equipe interna. (SEO Trends, 2017)
  
  - 66% dos profissionais de marketing dizem que melhorar SEO e crescer sua presença orgânica são a sua prioridade de Inbound Marketing. (HubSpot, 2016)
  
  - 50% das consultas de pesquisa têm quatro palavras ou mais. (WordStream, 2016)
  

# NEO
> No site oficial do produto (http://neo.jetecommerce.com.br/) encontramos a seguinte propaganda: SEO Friendly - Construída com as melhores práticas de SEO para que a loja tenha relevância nas buscas orgânicas.

 - A idéia do projeto é atender esse requisito com maestria, para torná-lo um ponto forte nos argumentos do setor comercial. E não menos importante, impulssionar as vendas das lojas migradas.


# 1 - SEO Básico
- Tags title, description e alt
![alt text](https://www.aguerradoseo.com.br/2017/wp-content/uploads/2017/05/seo-serp-tags.jpg)

- Exemplo "Made in Mato (Loja Publicada)"
![alt text](https://s9.postimg.org/p551npdq7/tag.png)    




# 2 - Snippets
 - Como o Google quer sempre entregar resultados de maior qualidade, e uma das formas é através da utilização dos rich snippets. Para isso, a ferramenta precisa da ajuda das marcações para identificar claramente as informações e entender melhor sobre o que é o conteúdo.

- Levantamento de rich snippets utilizados na loja "Made in Mato" com a ferramenta da google: http://www.google.com/webmasters/tools/richsnippets

![alt text](https://kek.gg/i/5LWHST.png)
- Lembrando que cada segmento de loja terá um 'schema' a ser seguido: http://schema.org/docs/schemas.html

- Exemplo de boa utilização retirado da documentação oficial (http://schema.org/docs/gs.html) para a propaganda de um filme:
```sh
<div itemscope itemtype ="http://schema.org/Movie">
  <h1 itemprop="name">Avatar</h1>
  <div itemprop="director" itemscope itemtype="http://schema.org/Person">
  Director: <span itemprop="name">James Cameron</span> (born <span itemprop="birthDate">August 16, 1954</span>)
  </div>
  <span itemprop="genre">Science fiction</span>
  <a href="../movies/avatar-theatrical-trailer.html" itemprop="trailer">Trailer</a>
</div>
```

# 3 - Mobile First
 - Notícia do blog da Google em 2016 (https://webmasters.googleblog.com/2016/11/mobile-first-indexing.html)
 
  Resumo(Retirado da Internet):
> O que isso significa é que o Google está começando a indexar a versão móvel dos >  sites quando disponível, para examinar as páginas móveis em relação aos sinais de  classificação para decidir como um site deve classificar nos resultados de pesquisa de desktop e móvel.

> Este desenvolvimento significa que as informações sobre o seu site móvel > (conteúdo, velocidade da página, dados estruturados, meta tags, etc.) em oposição à  versão desktop, serão usadas para determinar o ranking de sua área de trabalho e  celular no mecanismo de pesquisa do Google.

> De acordo com o Google, o raciocínio por trás desse desenvolvimento é que as > buscas móveis (em tablets, smartphones e alguns leitores eletrônicos) ultrapassaram as pesquisas de desktop há bastante tempo.

> Para garantir que o seu site seja considerado móvel amigável pelo Google, ele deve ter os seguintes recursos:

> Seu site deve evitar software, como o Flash, que não é comum em dispositivos móveis
O texto usado pelo seu site deve ser legível sem o zoom
O conteúdo do seu site deve ser dimensionado para a tela para garantir que os usuários não tenham que rolar horizontalmente ou ampliar para ler
Por último, todos os links no seu site devem ser separados o suficiente para que o correto possa ser facilmente aproveitado

 - Teste realizado na loja: "Made in Mato" com a ferramenta (https://search.google.com/test/mobile-friendly)

![alt text](https://kek.gg/i/5r8TdT.png )

- A loja está bem otimizada para mobile, com apenas pequenos ajustes a serem melhorados e testados, conforme imagem acima.

# 4 - Usar tags de título únicos para páginas de produtos
- Na plataforma NEO A tag <title> da Home é atribuida de forma manual, e nas páginas dinâmicas é substituido pelo nome do Produto apenas. Ex:
![alt text](https://kek.gg/i/4ngQfx.png)

- Uma maneira de melhorar isso seria usar o padrão: Marca - Modelo - Tipo 


# 5 - Redirecionamento de URLs de produtos expirados (301)
>Um redirecionamento permanente 301 é útil para redirecionar uma página para uma página de desvio.

>Eles são muito importantes para o SEO, porque eles permitem que você informe os mecanismos de pesquisa em que o seu conteúdo antigo se moveu.

>Com um redirecionamento 301, todas as qualidades da página redirecionada, como PageRank, autoridade de página, valor de tráfego, MozRank, etc., são movidas para a página de desvio.
> O mesmo vale para páginas específicas de campanhas ( Black Friday, Dia das Mães, Natal, etc)

# 6 - Robots.txt
 - Com esse arquivo é possível dizer aos mecanismos de busca as páginas que devem ser ignoradas nas buscas, ex: "Admin, admin b2c, etc"; Dessa forma os robôs economizam tempo ao indexar nossas páginas.
 - Ex prático de utilização retirado da internet: https://www.agenciamestre.com/seo/robots-txt/

# 7 - Otimizaçaõ com palavras-chave (
 - Através do  Google Webmaster Tools Analytics e demais ferramentas disponíveis é possível aprofundar aqui e realizar diversos testes para notar melhoras nas buscas. No entanto, desconheço a responsabilidade da JET sobre a loja após publicação e não sei se há algum setor de marketing digital que atue no pós venda; 
 

### Considerações Finais
 - Em primeiro lugar, não sou um especialista em Marketing Digital e esse documento deveria ser colaborativo, visto que apenas encontrei alguns pontos que compreendi que poderiam ser melhorados e numa breve pesquisa encontrei infinitas possibilidades e ferramentas que podem nos auxiliar; Seria interessante que todos avaliassem os pontos e fizessem modificações do que acham mais relevante na prática. Visto que todos da equipe possuem considerável experiência.


